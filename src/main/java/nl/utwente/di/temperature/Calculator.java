package nl.utwente.di.temperature;

public class Calculator {

    double getTemp(String temperature) {
        double cels = Integer.parseInt(temperature);
        return cels*33.8;
    }
}
