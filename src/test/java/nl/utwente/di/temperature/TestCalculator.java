package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestCalculator {
    @Test
    public void testBook1() throws Exception {
        Calculator calculator = new Calculator();
        double fahr = calculator.getTemp("1");
        Assertions.assertEquals(33.8, fahr, 0.0, "Tempoffahr1");
    }
}